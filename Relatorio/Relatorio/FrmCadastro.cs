﻿using Relatorio.DAL;
using Relatorio.MODEL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Relatorio.UI
{
    public partial class FrmCadastro : Form
    {
        //Declarando a DAL
        PessoasDAL pessoasDAL;

        //Somente para exibir mensagem de erro/sucesso
        public string msgSucesso;
        
        //nullable
        private int? codigo;

        public FrmCadastro(string titulo)
        {
            InitializeComponent();
            this.Text = titulo;

            //Intanciar a DAL na construção do formulário
            pessoasDAL = new PessoasDAL();
        }

        public FrmCadastro(string titulo, int codigo) 
            :this(titulo)
        {
            this.codigo = codigo;
            var pessoas = pessoasDAL.ConsultarPorCodigo(codigo);
            txtNome.Text = pessoas.Nome;
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            Pessoas pessoas = new Pessoas();
            pessoas.Nome = txtNome.Text;

            if (!codigo.HasValue)
            {
                pessoasDAL.Inserir(pessoas);
            }
            else
            {
                pessoas.Codigo = codigo.Value;
                pessoasDAL.Alterar(pessoas);
            }

            msgSucesso = $"Você efetuou o cadastro: \nNome: {txtNome.Text}";
            MessageBox.Show(msgSucesso, "Sucesso");
            
            //Fechando formulário da instância atual
            this.Close();
        }
    }
}
