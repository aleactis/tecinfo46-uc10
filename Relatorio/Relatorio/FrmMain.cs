﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Relatorio
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        private void pessoasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPessoas frmPessoas = new FrmPessoas();
            frmPessoas.MdiParent = this;
            frmPessoas.Show();
        }

        private void btnTestaConn_Click(object sender, EventArgs e)
        {
            //SqlConnection conn = new SqlConnection();
            //conn.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            //conn.Open();
            //MessageBox.Show("OBA! Conectei ao SQL Server via C#!!!", "Sucesso");
        }
    }
}
