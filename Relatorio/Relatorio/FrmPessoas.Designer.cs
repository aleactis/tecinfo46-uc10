﻿
namespace Relatorio
{
    partial class FrmPessoas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPesquisaNome = new System.Windows.Forms.Label();
            this.txtPesquisaNome = new System.Windows.Forms.TextBox();
            this.btnPesquisaNome = new System.Windows.Forms.Button();
            this.btnInserir = new System.Windows.Forms.Button();
            this.dgvListarPessoas = new System.Windows.Forms.DataGridView();
            this.btnRemovePessoa = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListarPessoas)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPesquisaNome
            // 
            this.lblPesquisaNome.AutoSize = true;
            this.lblPesquisaNome.Location = new System.Drawing.Point(20, 45);
            this.lblPesquisaNome.Name = "lblPesquisaNome";
            this.lblPesquisaNome.Size = new System.Drawing.Size(40, 15);
            this.lblPesquisaNome.TabIndex = 0;
            this.lblPesquisaNome.Text = "Nome";
            // 
            // txtPesquisaNome
            // 
            this.txtPesquisaNome.Location = new System.Drawing.Point(75, 37);
            this.txtPesquisaNome.Name = "txtPesquisaNome";
            this.txtPesquisaNome.Size = new System.Drawing.Size(310, 23);
            this.txtPesquisaNome.TabIndex = 1;
            // 
            // btnPesquisaNome
            // 
            this.btnPesquisaNome.Location = new System.Drawing.Point(503, 37);
            this.btnPesquisaNome.Name = "btnPesquisaNome";
            this.btnPesquisaNome.Size = new System.Drawing.Size(75, 23);
            this.btnPesquisaNome.TabIndex = 2;
            this.btnPesquisaNome.Text = "Pesquisar";
            this.btnPesquisaNome.UseVisualStyleBackColor = true;
            this.btnPesquisaNome.Click += new System.EventHandler(this.btnPesquisaNome_Click);
            // 
            // btnInserir
            // 
            this.btnInserir.Location = new System.Drawing.Point(668, 22);
            this.btnInserir.Name = "btnInserir";
            this.btnInserir.Size = new System.Drawing.Size(105, 38);
            this.btnInserir.TabIndex = 3;
            this.btnInserir.Text = "Nova Pessoa";
            this.btnInserir.UseVisualStyleBackColor = true;
            this.btnInserir.Click += new System.EventHandler(this.btnInserir_Click);
            // 
            // dgvListarPessoas
            // 
            this.dgvListarPessoas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListarPessoas.Location = new System.Drawing.Point(15, 132);
            this.dgvListarPessoas.Name = "dgvListarPessoas";
            this.dgvListarPessoas.RowTemplate.Height = 25;
            this.dgvListarPessoas.Size = new System.Drawing.Size(625, 283);
            this.dgvListarPessoas.TabIndex = 4;
            this.dgvListarPessoas.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvListarPessoas_CellContentClick);
            this.dgvListarPessoas.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvListarPessoas_CellDoubleClick);
            // 
            // btnRemovePessoa
            // 
            this.btnRemovePessoa.Location = new System.Drawing.Point(668, 132);
            this.btnRemovePessoa.Name = "btnRemovePessoa";
            this.btnRemovePessoa.Size = new System.Drawing.Size(105, 38);
            this.btnRemovePessoa.TabIndex = 3;
            this.btnRemovePessoa.Text = "Remove Pessoa";
            this.btnRemovePessoa.UseVisualStyleBackColor = true;
            this.btnRemovePessoa.Click += new System.EventHandler(this.btnRemovePessoa_Click);
            // 
            // FrmPessoas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dgvListarPessoas);
            this.Controls.Add(this.btnRemovePessoa);
            this.Controls.Add(this.btnInserir);
            this.Controls.Add(this.btnPesquisaNome);
            this.Controls.Add(this.txtPesquisaNome);
            this.Controls.Add(this.lblPesquisaNome);
            this.Name = "FrmPessoas";
            this.Text = "Pessoas";
            this.Load += new System.EventHandler(this.FrmPessoas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListarPessoas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPesquisaNome;
        private System.Windows.Forms.TextBox txtPesquisaNome;
        private System.Windows.Forms.Button btnPesquisaNome;
        private System.Windows.Forms.Button btnInserir;
        private System.Windows.Forms.DataGridView dgvListarPessoas;
        private System.Windows.Forms.Button btnRemovePessoa;
    }
}