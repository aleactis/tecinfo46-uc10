﻿using Relatorio.DAL;
using Relatorio.MODEL;
using Relatorio.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Relatorio
{
    public partial class FrmPessoas : Form
    {
        PessoasDAL pessoasDAL = new PessoasDAL();

        public FrmPessoas()
        {
            InitializeComponent();
        }

        private void btnInserir_Click(object sender, EventArgs e)
        {
            //Form SDI
            FrmCadastro cadastro = new FrmCadastro("Minha tela de cadastro");
            cadastro.ShowDialog();

            //Preencher a grid de listagem
            PreencherGrid();
        }

        private void PreencherGrid()
        {
            dgvListarPessoas.DataSource = null;
            dgvListarPessoas.DataSource = pessoasDAL.Listar();
        }

        private void FrmPessoas_Load(object sender, EventArgs e)
        {
            PreencherGrid();
        }

        private void dgvListarPessoas_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //Casting para pessoas (conversão explícita)
            var pessoa = (Pessoas)dgvListarPessoas.Rows[e.RowIndex].DataBoundItem;
            FrmCadastro cadastro = new FrmCadastro("Minha Tela de Alteração", pessoa.Codigo);
            cadastro.ShowDialog();
            PreencherGrid();
        }

        private void dgvListarPessoas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnRemovePessoa_Click(object sender, EventArgs e)
        {
            //Casting
            var pessoa = (Pessoas)dgvListarPessoas.SelectedRows[0].DataBoundItem;

            //Dialog Result
            string msg = "Confirma a exclusão da pessoa " + pessoa.Nome;
            string caption = "Remove Pessoas";
            var resultado = MessageBox.Show(msg, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (resultado == DialogResult.Yes)
            {
                pessoasDAL.Excluir(pessoa.Codigo);
                PreencherGrid();
            }
        }

        private void btnPesquisaNome_Click(object sender, EventArgs e)
        {
            dgvListarPessoas.DataSource = null;
            dgvListarPessoas.DataSource = pessoasDAL.Pesquisar(txtPesquisaNome.Text);
        }
    }
}
