﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Relatorio.MODEL
{
    public class Pessoas
    {

        #region Propriedades
        public int Codigo { get; set; }
        public string Nome { get; set; }
        #endregion

        #region Construtor Padrão
        public Pessoas()
        {

        }
        #endregion
    }
}
