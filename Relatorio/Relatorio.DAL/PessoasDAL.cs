﻿using Relatorio.MODEL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Relatorio.DAL
{
    public class PessoasDAL
    {
        private SqlConnection conexao;
        public string MensagemErro { get; set; }

        #region Construtor Padrão
        public PessoasDAL()
        {
            //Criar os objetos para ler a configuração
            LeitorConfiguracao leitor = new LeitorConfiguracao();

            //Instanciar a conexão
            conexao = new SqlConnection();
            conexao.ConnectionString = leitor.LerConexao();
        }
        #endregion

        #region Inserir
        //CRUD - Create/Read/Update/Delete

        //Letra C do Crud
        public bool Inserir(Pessoas pessoas)
        {
            bool sucesso = false;
            //Limpar mensagem de erro
            MensagemErro = "";

            //Declaração de comandos DML
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao;
            cmd.CommandText = @"INSERT INTO pessoas (Nome) VALUES (@nome)";

            //Criar os parâmetros de acesso aos dados
            cmd.Parameters.AddWithValue("@Nome", pessoas.Nome);

            //Executar o comando
            try
            {
                //Abrindo a conexão
                conexao.Open();

                //Executar comando T-SQL e retorna o número de linhas afetadas
                cmd.ExecuteNonQuery();

                //Se chegou até aqui, então tá sussa!
                sucesso = true;
            }
            catch (Exception ex)
            {
                //Se entrou aqui, então a casa caiu... kkkk
                MensagemErro = ex.Message;
            }
            finally
            {
                //Fechando a conexão com o SGBD
                conexao.Close();
            }
            return sucesso;
        }
        #endregion

        #region Listar
        public List<Pessoas> Listar()
        {
            //Instanciar a lista de pessoas
            List<Pessoas> listaPessoas = new List<Pessoas>();

            //Declarar o comando
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao;
            cmd.CommandText = @"SELECT * FROM Pessoas ORDER BY Codigo, Nome";

            //Executar o comando
            try
            {
                //Abrir a conexão
                conexao.Open();

                //Executar o comando e receber o resultado
                SqlDataReader leitor = cmd.ExecuteReader();

                //Verificar se encontrou algo
                while (leitor.Read() == true)
                {
                    //Instanciar os objetos
                    Pessoas pessoas = new Pessoas();
                    pessoas.Nome = leitor["Nome"].ToString();
                    pessoas.Codigo = Convert.ToInt32(leitor["Codigo"]);

                    //Adicionar os objs na lista
                    listaPessoas.Add(pessoas);
                }

                //Fechar o leitor
                leitor.Close();
            }
            catch (Exception ex)
            {
                //Se entrou aqui, então deu erro!
                MensagemErro = ex.Message;
            }
            finally
            {
                //Finalizar a conexão
                conexao.Close();
            }
            return listaPessoas;
        }
        #endregion

        #region Consultar por Codigo

        public Pessoas ConsultarPorCodigo(int codigo)
        {
            Pessoas pessoas = null;

            //Limpar mensagem de erro
            MensagemErro = "";

            //Declarar o comando
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao;
            cmd.CommandText = @"SELECT * FROM Pessoas WHERE Codigo = @Codigo";

            //Criar os parâmetros
            cmd.Parameters.AddWithValue("@Codigo", codigo);

            //Executar o comando
            try
            {
                //Abrir a conexão
                conexao.Open();

                //Executar o comando e receber o resultado
                SqlDataReader leitor = cmd.ExecuteReader();

                //Verificar se encontrou algo
                if (leitor.Read() == true)
                {
                    //Instancio o objeto
                    pessoas = new Pessoas();
                    pessoas.Codigo = Convert.ToInt32(leitor["Codigo"].ToString());
                    pessoas.Nome = leitor["Nome"].ToString();
                }

                //Fechar o leitor
                leitor.Close();
            }
            catch (Exception ex)
            {
                //Se entrou aqui, então deu erro!
                MensagemErro = ex.Message;
            }
            finally
            {
                //Finalizar a conexão
                conexao.Close();
            }
            return pessoas;
        }
        #endregion

        #region Alterar
        public bool Alterar(Pessoas pessoas)
        {
            bool sucesso = false;
            MensagemErro = "";

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao;
            cmd.CommandText = @"UPDATE Pessoas
                                SET Nome = @Nome
                                WHERE Codigo = @Codigo";

            cmd.Parameters.AddWithValue("@Codigo", pessoas.Codigo);
            cmd.Parameters.AddWithValue("@Nome", pessoas.Nome);

            try
            {
                conexao.Open();
                cmd.ExecuteNonQuery();
                sucesso = true;
            }
            catch (Exception ex)
            {
                MensagemErro = ex.Message;
            }finally
            {
                conexao.Close();
            }
            return sucesso;
        }
        #endregion

        #region Excluir 
        public bool Excluir(int codigo)
        {
            bool sucesso = false;
            MensagemErro = "";

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao;
            cmd.CommandText = @"DELETE FROM Pessoas WHERE Codigo = @Codigo";

            cmd.Parameters.AddWithValue("@Codigo", codigo);

            try
            {
                conexao.Open();
                cmd.ExecuteNonQuery();
                sucesso = true;
            }
            catch (Exception ex)
            {
                MensagemErro = ex.Message;
            }
            finally
            {
                conexao.Close();
            }
            return sucesso;
        }
        #endregion

        #region Pesquisar 
        public List<Pessoas> Pesquisar(string nome)
        {
            List<Pessoas> listaPessoas = new List<Pessoas>();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conexao;
            cmd.CommandText = "SELECT * FROM pessoas WHERE Nome LIKE @Nome + '%' ";

            cmd.Parameters.AddWithValue("@Nome", nome);

            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();

                while (leitor.Read() == true)
                {
                    Pessoas pessoas = new Pessoas();
                    pessoas.Codigo = Convert.ToInt32(leitor["Codigo"]);
                    pessoas.Nome = leitor["Nome"].ToString();

                    listaPessoas.Add(pessoas);
                }
                leitor.Close();
            }
            catch (SqlException ex)
            {
                MensagemErro = ex.Message;
            } finally
            {
                conexao.Close();
            }
            return listaPessoas;
        }
        #endregion
    }
}
