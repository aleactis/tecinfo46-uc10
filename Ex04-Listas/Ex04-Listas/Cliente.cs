﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex04_Listas
{
    public class Cliente
    {
        //string nome; campo

        //Auto-Properties
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public int Idade { get; set; }
    }
}


//Criar 02 classes
//1. Cidade
    //1.1- Nome

//2. Estado
    //2.1- ID
    //2.2- UF