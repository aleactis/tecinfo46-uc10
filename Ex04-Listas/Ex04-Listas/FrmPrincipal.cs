﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ex04_Listas
{
    public partial class FrmPrincipal : Form
    {
        //Declara a lista
        List<Cliente> listaClientes;
        
        //Declarando e Instanciando a lista de estados
        List<Estado> listaEstados = new List<Estado>();

        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void btnConfirma_Click(object sender, EventArgs e)
        {
            //Instanciar a lista 
            listaClientes = new List<Cliente>
            {
                new Cliente {Id = 1, Nome ="Julia", Email="julia@gmail.com",Idade=17},
                new Cliente {Id=2, Nome ="Reginaldo", Email="reginaldo@hotmail.com", Idade=18},
                new Cliente {Id=3, Nome="Pedro", Email="pedro@gmail.com", Idade=19},
                new Cliente {Id=4, Nome="Patrick", Email="erick@gmail.com", Idade=20},
                new Cliente {Id=5, Nome="William", Email="willian@gmail.com", Idade=20},
                new Cliente {Id=6, Nome="Gabriel", Email="gabriel@gmail.com", Idade=21},
                new Cliente {Id=7, Nome="Alexandre", Email="alexandre@gmail.com", Idade=22}
            };

            //Popular o datagridview
            dgvListaPessoas.DataSource = listaClientes;

            lblQuantidade.Text = listaClientes.Count.ToString() + " alunos";
        }

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {
            //Carregar o combobox
            listaEstados.Add(new Estado() { Id=1, Uf="São Paulo"});
            listaEstados.Add(new Estado() { Id = 2, Uf = "Rio de Janeiro" });
            listaEstados.Add(new Estado() { Id = 3, Uf = "Amazonas" });
            cbEstado.DataSource = listaEstados;
            cbEstado.ValueMember = "ID";
            cbEstado.DisplayMember = "UF";
        }

        private void cbEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblEstado.Text = cbEstado.Text;
        }
    }
}
