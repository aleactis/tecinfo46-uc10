﻿using System;
using System.Windows.Forms;

namespace Aula02
{ 
    public partial class frmPrincipal : Form
    {
        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            //MessageBox.Show("Hello World!!");
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(txtNome.Text, "Mensagem");

            textBox1.Text = txtNome.Text;
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            //Application.Exit();
            this.Close();
        }

        private void frmPrincipal_Shown(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
